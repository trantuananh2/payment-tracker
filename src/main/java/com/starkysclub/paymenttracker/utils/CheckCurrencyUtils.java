package com.starkysclub.paymenttracker.utils;

import java.util.Currency;

/**
 * CheckCurrencyUtils is used for working with currency
 * @author Ivan Tran, tran.tuan.anh@starkysclub.com
 * 28.08.2019
 */
public final class CheckCurrencyUtils {

    private CheckCurrencyUtils() {
    }

    /**
     * checkCurrency is used for checking currency
     * @param currency
     */
    public static boolean checkCurrency(String currency) {
        if (!currency.isEmpty()) {
            String currencyLocale = "";
            try {
                currencyLocale = Currency.getInstance(currency.toUpperCase()).getCurrencyCode();
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
            return currency.equals(currencyLocale);
        } else {
            return false;
        }
    }
}
