package com.starkysclub.paymenttracker;

import com.starkysclub.paymenttracker.service.PaymentTrackerService;
import com.starkysclub.paymenttracker.service.impl.PaymentTrackerServiceImpl;
import com.starkysclub.paymenttracker.utils.CheckCurrencyUtils;

import java.util.Scanner;

/**
 *
 * @author Ivan Tran, tran.tuan.anh@starkysclub.com
 * 28.08.2019
 */
public class PaymentTrackerApplication {



    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("\n" +
                "$$$$$$$\\                                                        $$\\             $$\\                                  $$\\                                  \n" +
                "$$  __$$\\                                                       $$ |            $$ |                                 $$ |                                 \n" +
                "$$ |  $$ |$$$$$$\\  $$\\   $$\\ $$$$$$\\$$$$\\   $$$$$$\\  $$$$$$$\\ $$$$$$\\         $$$$$$\\    $$$$$$\\  $$$$$$\\   $$$$$$$\\ $$ |  $$\\  $$$$$$\\   $$$$$$\\         \n" +
                "$$$$$$$  |\\____$$\\ $$ |  $$ |$$  _$$  _$$\\ $$  __$$\\ $$  __$$\\\\_$$  _|        \\_$$  _|  $$  __$$\\ \\____$$\\ $$  _____|$$ | $$  |$$  __$$\\ $$  __$$\\        \n" +
                "$$  ____/ $$$$$$$ |$$ |  $$ |$$ / $$ / $$ |$$$$$$$$ |$$ |  $$ | $$ |            $$ |    $$ |  \\__|$$$$$$$ |$$ /      $$$$$$  / $$$$$$$$ |$$ |  \\__|       \n" +
                "$$ |     $$  __$$ |$$ |  $$ |$$ | $$ | $$ |$$   ____|$$ |  $$ | $$ |$$\\         $$ |$$\\ $$ |     $$  __$$ |$$ |      $$  _$$<  $$   ____|$$ |             \n" +
                "$$ |     \\$$$$$$$ |\\$$$$$$$ |$$ | $$ | $$ |\\$$$$$$$\\ $$ |  $$ | \\$$$$  |        \\$$$$  |$$ |     \\$$$$$$$ |\\$$$$$$$\\ $$ | \\$$\\ \\$$$$$$$\\ $$ |             \n" +
                "\\__|      \\_______| \\____$$ |\\__| \\__| \\__| \\_______|\\__|  \\__|  \\____/          \\____/ \\__|      \\_______| \\_______|\\__|  \\__| \\_______|\\__|             \n" +
                "                   $$\\   $$ |                                                                                                                             \n" +
                "                   \\$$$$$$  |                                                                                                                             \n" +
                "                    \\______/                                                                                                                              \n" +
                "            $$\\                         $$\\                                           $$\\           $$\\                                                   \n" +
                "            $$ |                        $$ |                                          $$ |          $$ |                                                  \n" +
                " $$$$$$$\\ $$$$$$\\    $$$$$$\\   $$$$$$\\  $$ |  $$\\ $$\\   $$\\  $$$$$$$\\        $$$$$$$\\ $$ |$$\\   $$\\ $$$$$$$\\             $$$$$$$\\  $$$$$$\\  $$$$$$\\$$$$\\  \n" +
                "$$  _____|\\_$$  _|   \\____$$\\ $$  __$$\\ $$ | $$  |$$ |  $$ |$$  _____|      $$  _____|$$ |$$ |  $$ |$$  __$$\\           $$  _____|$$  __$$\\ $$  _$$  _$$\\ \n" +
                "\\$$$$$$\\    $$ |     $$$$$$$ |$$ |  \\__|$$$$$$  / $$ |  $$ |\\$$$$$$\\        $$ /      $$ |$$ |  $$ |$$ |  $$ |          $$ /      $$ /  $$ |$$ / $$ / $$ |\n" +
                " \\____$$\\   $$ |$$\\ $$  __$$ |$$ |      $$  _$$<  $$ |  $$ | \\____$$\\       $$ |      $$ |$$ |  $$ |$$ |  $$ |          $$ |      $$ |  $$ |$$ | $$ | $$ |\n" +
                "$$$$$$$  |  \\$$$$  |\\$$$$$$$ |$$ |      $$ | \\$$\\ \\$$$$$$$ |$$$$$$$  |      \\$$$$$$$\\ $$ |\\$$$$$$  |$$$$$$$  |$$\\       \\$$$$$$$\\ \\$$$$$$  |$$ | $$ | $$ |\n" +
                "\\_______/    \\____/  \\_______|\\__|      \\__|  \\__| \\____$$ |\\_______/        \\_______|\\__| \\______/ \\_______/ \\__|       \\_______| \\______/ \\__| \\__| \\__|\n" +
                "                                                  $$\\   $$ |                                                                                              \n" +
                "                                                  \\$$$$$$  |                                                                                              \n" +
                "                                                   \\______/                                                                                               \n");
        int time = 60000;
        int delay = 0;
        PaymentTrackerService paymentTrackerService = new PaymentTrackerServiceImpl();
        paymentTrackerService.statementOfAccountWithTime(time, delay);
        while (true) {
            String value = scanner.nextLine();
            if (value.equals("quit")) {
                System.exit(0);
            }
            String[] parts = value.split(" ");
            String currency = parts[0];
            int amount = Integer.parseInt(parts[1]);
            if (CheckCurrencyUtils.checkCurrency(currency)) {
                paymentTrackerService.addPayment(amount, currency);
            }else {
                System.exit(0);
            }
        }
    }

}
