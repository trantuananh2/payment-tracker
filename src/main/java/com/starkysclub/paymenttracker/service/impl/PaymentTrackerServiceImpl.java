package com.starkysclub.paymenttracker.service.impl;

import com.starkysclub.paymenttracker.model.Payment;
import com.starkysclub.paymenttracker.model.PaymentTracker;
import com.starkysclub.paymenttracker.service.PaymentTrackerService;

import javax.money.Monetary;
import javax.money.MonetaryAmount;
import javax.money.convert.CurrencyConversion;
import javax.money.convert.MonetaryConversions;
import java.util.Timer;
import java.util.TimerTask;

/**
 *
 * @author Ivan Tran, tran.tuan.anh@starkysclub.com
 * 29.08.2019
 */
public class PaymentTrackerServiceImpl implements PaymentTrackerService {

    private PaymentTracker paymentTracker = new PaymentTracker();


    /**
     * Add a payment
     * @param amount
     * @param currency
     */
    @Override
    public void addPayment(int amount, String currency) {
            if (findCurrency(currency)) {
                for (int i = 0; i < paymentTracker.getPaymentArrayList().size(); i++) {
                    if (currency.equals(paymentTracker.getPaymentArrayList().get(i).getCurrency())) {
                        Payment payment = paymentTracker.getPaymentArrayList().get(i);
                        int suma = payment.getValue() + amount;
                        payment.setValue(suma);
                        paymentTracker.getPaymentArrayList().set(i, payment);
                    }
                }
            } else {
                Payment payment = new Payment();
                payment.setValue(amount);
                payment.setCurrency(currency);
                paymentTracker.getPaymentArrayList().add(payment);
            }
    }

    /**
     * Find currency in Payment Tracker if there is return true
     */
    private boolean findCurrency(String currency) {
        boolean checkCurrency = paymentTracker.getPaymentArrayList().stream().filter(payment-> currency.equals(payment.getCurrency())).findFirst().isPresent();
        return checkCurrency;
    }

    /**
     * Statement of account with time
     */
    @Override
    public void statementOfAccountWithTime(int period, int delay) {
        Timer timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            public void run() {
                statementOfAccount();
            }
        }, delay, period);
    }

    @Override
    public Payment findById(int id) {
        return paymentTracker.getPaymentArrayList().get(id);
    }

    /**
     * Statement of account
     */
    private void statementOfAccount() {
        for (Payment payment : paymentTracker.getPaymentArrayList()) {
            if (payment.getCurrency().equals("USD")) {
                System.out.println(payment.getCurrency() + " " + payment.getValue());
            } else {
                comparedCurrencyWithUSD(payment);
            }
        }
    }


    /*
        Allow each currency to have the exchange rate compared to USD configured. When you display the output,
        write the USD equivalent amount next to it,
     */
    private void comparedCurrencyWithUSD(Payment payment) {
        MonetaryAmount currency = Monetary.getDefaultAmountFactory().setCurrency(payment.getCurrency()).setNumber(Long.valueOf(payment.getValue())).create();
        CurrencyConversion conversionUSD = MonetaryConversions.getConversion("USD");
        //convert all currency to USD need time then we use Thread sleep
        try {
            Thread.sleep(1000L);
        } catch (InterruptedException e) {
            throw new RuntimeException();
        }
        MonetaryAmount convertedAmountCurrencyToUSD = currency.with(conversionUSD);
        double number = Double.valueOf(convertedAmountCurrencyToUSD.getNumber().toString());
        double roundOff = Math.round(number * 100.0) / 100.0;
        System.out.println(payment.getCurrency() + " " + payment.getValue() + "(USD " + roundOff + ")");
    }

}
