package com.starkysclub.paymenttracker.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.ArrayList;

/**
 * @author Ivan Tran, tran.tuan.anh@starkysclub.com
 * 28.08.2019
 */
public class PaymentTracker {

    private ArrayList<Payment> paymentArrayList = new ArrayList<>();

    public ArrayList<Payment> getPaymentArrayList() {
        return paymentArrayList;
    }

}
