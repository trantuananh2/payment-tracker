package com.starkysclub.paymenttracker.service;

import com.starkysclub.paymenttracker.model.Payment;

/**
 * {@link PaymentTrackerService} is  used for working with payment
 * @author Ivan Tran, tran.tuan.anh@starkysclub.com
 * 29.08.2019
 */
public interface PaymentTrackerService {
    void addPayment(int amount, String currency);
    void statementOfAccountWithTime(int period, int delay);
    Payment findById(int id);
}
