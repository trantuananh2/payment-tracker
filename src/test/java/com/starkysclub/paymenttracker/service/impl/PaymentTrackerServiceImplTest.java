package com.starkysclub.paymenttracker.service.impl;

import com.starkysclub.paymenttracker.model.Payment;
import com.starkysclub.paymenttracker.service.PaymentTrackerService;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * @author Ivan Tran, tran.tuan.anh@starkysclub.com
 * 01.09.2019
 */
public class PaymentTrackerServiceImplTest {

    private PaymentTrackerService paymentTrackerService;

    @Test
    public void paymentTrackerServiceTest() {
        paymentTrackerService = new PaymentTrackerServiceImpl();
        paymentTrackerService.addPayment(100, "CZK");
        Payment payment = new Payment();
        payment.setValue(100);
        payment.setCurrency("CZK");
        Payment payment1 = paymentTrackerService.findById(0);
        assertEquals(payment.getCurrency(), payment1.getCurrency());
        assertEquals(payment.getValue(), payment1.getValue());
    }
}
