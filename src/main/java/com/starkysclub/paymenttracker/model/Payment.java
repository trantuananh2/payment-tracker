package com.starkysclub.paymenttracker.model;

/**
 * @author Ivan Tran, tran.tuan.anh@starkysclub.com
 * 28.08.2019
 */
public class Payment {

    private int value;
    private String currency;

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }
}
